// - - - - - - - CONVERSION DE NUMERO DECIMAL A NUMERO MAYA - - - - - - -
//Variables
var decimal;
var n1 = 1;
var n2 = 20;
var n3 = 400;
//obtener el numero decimal
function numDecimal(){
    decimal = document.getElementById('dec').value;
    if (decimal == ''){
        decimal = null;
    }
    return decimal;
}
//Division
function division(valor, div){
    var resultado = Math.trunc(valor/div);
    //console.log(resultado);
    return resultado;
}
//Multiplicacion
function producto(valor, num){
    var mult;
    mult = valor * num;
    return mult;
}
//Resta
function resta(valor){
    var resta;
    resta = numDecimal() - valor;
    return resta;
}
//Convertir a numero Maya
function maya(){
    var numero = numDecimal();
    if (numero != null){
        console.log ("Número a representar:"+numero);
        //empieza procedimiento
        if (numero <= 7999){ 
            //Las variables "vn" representan el cociente de la división
            var vn3= division(numero,n3);
            var vn2;
            var vn1; 
            var x; //Variable modificable para las operaciones de residuos
            //Nivel 3
            console.log("Valor nivel 3:"+vn3);
            if ((vn3 >= 1) && (vn3 <= 19)){
                x = resta(producto(vn3, n3));
                vn2 = division(x, n2);
                //Nivel 2
                console.log("Valor nivel 2:"+vn2);
                if ((vn2 >= 1) && (vn2 <= 19)){
                    x = resta((producto(vn2, n2)) + (producto(vn3, n3)));
                    vn1 = division(x, n1);
                    //Nivel 1
                    console.log("Valor nivel 1:"+vn1);
                }
                else{
                    vn1 = division(x, n1);
                    //Nivel 1
                    console.log("Valor nivel 1:"+vn1);
                }
            }
            else{
                vn2 = division(numero, n2);
                //Nivel 2
                console.log("Valor nivel 2:"+vn2);
                if ((vn2 >= 1) && (vn2 <= 19)){
                    x = resta(producto(vn2, n2));
                    vn1 =  division(x, n1);
                    //Nivel 1
                    console.log("Valor nivel 1:"+vn1);
                }
                else{
                    vn1 =  division(numero, n1);
                    //Nivel 2
                    console.log("Valor nivel 1:"+vn1);
                }
            }
            //Validaciones con imagenes
            if ((vn2 == 0) && (vn3 == 0)){
                document.getElementById("nivel3").src = "./img/blanco.png";
                document.getElementById("nivel2").src = "./img/blanco.png";
                document.getElementById("nivel1").src = "./img/"+vn1+".png";
            }
            else if(vn3 == 0){
                document.getElementById("nivel3").src = "./img/blanco.png";
                document.getElementById("nivel2").src = "./img/"+vn2+".png";
                document.getElementById("nivel1").src = "./img/"+vn1+".png";
            }
            else{
                document.getElementById("nivel3").src = "./img/"+vn3+".png";
                document.getElementById("nivel2").src = "./img/"+vn2+".png";
                document.getElementById("nivel1").src = "./img/"+vn1+".png";
            }
        }//if num menor igual 7999
        else{
            alert("No se reconoce el número, favor de ingresar un numero en el rango 0-7999");
            console.log ("No se reconoce el número, favor de ingresar un numero en el rango 0-7999");
        }
        //termina validación
    }//num null
    else{
        console.log("Favor de ingresar un valor");
        alert("Favor de ingresar un valor");
    }
}//fin funcion maya()