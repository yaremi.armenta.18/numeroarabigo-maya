// - - - - - - - CONVERSION DE NUMERO DECIMAL A NUMERO MAYA - - - - - - -
//Variables
var decimal;
var n1 = 1;
var n2 = 20;
//obtener el numero decimal
function numDecimal(){
    decimal = document.getElementById('dec').value;
    return decimal;
}
//Division
function division(valor, div){
    var resultado = Math.trunc(valor/div);
    //console.log(resultado);
    return resultado;
}
//Multiplicacion
function producto(valor, num){
    var mult;
    mult = valor * num;
    return mult;
}
//Resta
function resta(valor){
    var resta;
    resta = numDecimal() - valor;
    return resta;
}
//Convertir a numero Maya
function maya(){
    var numero = numDecimal();
    console.log ("Número a representar:"+numero);
    if (numero <= 20){
        //Empieza validación
        //Las variables "vn" representan el cociente de la división
        var vn2= division(numero,n2);
        var vn1; 
        var x; //Variable modificable para las operaciones de residuos
        console.log("Valor nivel 2:"+vn2);
        if ((vn2 >= 1) && (vn2 <= 19)){
            x = resta(producto(vn2, n2));
            vn1 = x;
            console.log("Valor nivel 1:"+vn1);
        }
        else{
            vn1 = numero;
            console.log("Valor nivel 1:"+vn1);
        }
        //Termina validación
    }//if num menor igual 20
    else{
        console.log ("No se reconoce el número, favor de ingresar un numero en el rango 0-20");
    }
}