//Variables
var n3 = 400;
var n2 = 20;
var n1 = 1;
var nivel3 = null;
var nivel2 = null;
var nivel1 = null;
var clic = 0;

//Reconoce el numero a transformar y cambia imagen
function numM(num){
 if(clic == 0){//nivel 1
   nivel1 = num;
   document.getElementById('v1').src = "../img/"+nivel1+".png";
   document.getElementById('error').innerHTML = "";
   document.getElementById('nivel').innerHTML = "Selecciona valor para el nivel 2";
   console.log("Nivel 1="+nivel1);
   clic++;
 }
 else if (clic == 1){//nivel 2
   nivel2 = num;
    console.log("Nivel 2="+nivel2);
    document.getElementById('v2').src = "../img/"+nivel2+".png";
    document.getElementById('error').innerHTML = "";
    document.getElementById('nivel').innerHTML = "Selecciona valor para el nivel 3";
    clic++;
 }
 else if(clic == 2){//nivel 3
   nivel3 = num;
   if((nivel1 >= 1) && (nivel2>=1) && (nivel3 >= 1)){
     console.log("El número es mayor a 400");
     //alert("El número es mayor a 400");
     document.getElementById('error').innerHTML = "El número es mayor a 400";
     nivel3 = null;
     //console.log("Nivel 3="+nivel3);
   }
   else if((nivel1 >= 1) && (nivel2>=1) && (nivel3 == 0)){
     nivel3 = 0;
     console.log("Nivel 3="+nivel3);
     clic++;
    document.getElementById('error').innerHTML = "";
    document.getElementById('v3').src = "../img/blanco.png";
   }
   else if((nivel1==0) && (nivel2==0) && (nivel3==1)){
    clic++;
    document.getElementById('error').innerHTML = "";
    document.getElementById('v3').src = "../img/"+nivel3+".png";
  }
   else{
     if(nivel3 == null){
    document.getElementById('error').innerHTML = "";
    document.getElementById('v3').src = "../img/blanco.png";  
     }
     else if(nivel3 == 0){
    document.getElementById('error').innerHTML = "";
    document.getElementById('v3').src = "../img/blanco.png";
     }
     else{
    document.getElementById('error').innerHTML = "";
    document.getElementById('v3').src = "../img/"+nivel3+".png";
      console.log("Nivel 3="+nivel3);
     }
   }
 }
 else{
  //alert("Los campos ya estan llenos");
   document.getElementById('error').innerHTML = "Los campos estan llenos";
 }
}

function convertirM(){
    var decimal;
    if ((nivel1 == null) && (nivel2 ==null) && (nivel3 == null)){
      //alert('Por favor llenar los campos necesarios');
      document.getElementById('error').innerHTML = "Por favor, ingrese valor por lo menos para un nivel";
    }
    else{
      decimal  = (nivel1*n1) + (nivel2*n2) + (nivel3*n3);
      document.getElementById('msj').innerHTML = decimal;
      document.getElementById('error').innerHTML = "";
      document.getElementById('nivel').innerHTML = "";
      console.log(decimal);
    }
}

function limpiar(){
  clic = 0;
  nivel1 = null;
  nivel2 = null;
  nivel3 = null;
  document.getElementById('v1').src = "../img/blanco.png";
  document.getElementById('v2').src = "../img/blanco.png";
  document.getElementById('v3').src = "../img/blanco.png";
  document.getElementById('msj').innerHTML = "";  
  document.getElementById('error').innerHTML = "";
  document.getElementById('nivel').innerHTML = "Selecciona valor para el nivel 1";
}